var express = require('express');
var serve_static = require('serve-static');
var http = require('http');
const { fstat } = require('fs');
const { REPL_MODE_STRICT } = require('repl');

var app = express();

//Activation du serveur statique
app.use(serve_static(__dirname+"/public"));

// Récupération du serveur http de l'application
var serveur = http.Server(app);

//Ecoute sur un seul port
serveur.listen(8080, function()
{
    console.log("Serveur en écoute sur le port 8080");
});

var waitingRoom = undefined;

var CreateGameRoom = () => {
    let game = {};
    game.nodes = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
    game.current = 0;
    game.play = (pid, x, y) => {
        if (x < 0 || x >= 3 || y < 0 || y >= 3 || pid < 0 || pid > 1) return false;
        if (game.current != pid || game.nodes[y][x] != 0) return false;
        game.nodes[y][x] = pid + 1;
        game.current = 1 - game.current;
        return true;
    };
    game.isFinished = () => {
        let res;
        for (let i = 0; i < 3; i++) {
            res = game.nodes[i][0] * game.nodes[i][1] * game.nodes[i][2];
            if (res == 1)   return 1;
            else if (res == 8)  return 0;
        }

        for (let i = 0; i < 3; i++) {
            res = game.nodes[0][i] * game.nodes[1][i] * game.nodes[2][i];
            if (res == 1)   return 1;
            else if (res == 8)  return 0;
        }

        res = game.nodes[0][0] * game.nodes[1][1] * game.nodes[2][2];
        if (res == 1)   return 1;
        else if (res == 8)  return 0;
        res = game.nodes[0][2] * game.nodes[1][1] * game.nodes[2][0];
        if (res == 1)   return 1;
        else if (res == 8)  return 0;

        res = 1;
        for (let i = 0; i < 3; i++)
            for(let j = 0; j < 3; j++)
                res *= game.nodes[i][j];

        if (res != 0)
            return 2;

        return -1;

    };
    return game;
};

//Gestion du temps réel
var io = require('socket.io').listen(serveur);
io.sockets.on('connection', function (socket) {
    socket.on('disconnect', function(){
        if (socket.room) {
            socket.room.opponent.emit('opponent-disconnected');
            socket.room.opponent.emit('server-message', `${socket.username} s'est déconnecté.`);
        }
    });

    socket.on('register-user', (username) => {
        if (!username || username.length < 3 || username.length > 15) {
            socket.emit('server-message', 'Votre nom d\'utilisateur n\'est pas valide');
            return;
        }

        if (!socket.username) {
            socket.username = username;
            socket.emit('server-message', `Bienvenue ${username} !`);
        }
        if (waitingRoom === undefined) {
            waitingRoom = socket;
            socket.emit('server-message', 'En attente d\'un adversaire...');
        } else {
            let other = waitingRoom;
            waitingRoom = undefined;

            let game = CreateGameRoom();
            other.room = {
                pid: 0,
                player: other,
                opponent: socket,
                game: game
            };
            socket.room = {
                pid: 1,
                player: socket,
                opponent: other,
                game: game
            };

            socket.emit('start-game');
            other.emit('start-game');

            other.emit('server-message', `Match trouvé ! Vous jouez contre ${socket.username}. (Vous êtes les cercles)`);
            other.emit('server-message', 'A vous de jouer !');
            socket.emit('server-message', `Match trouvé ! Vous jouez contre ${other.username}. (Vous êtes les croix)`);
            socket.emit('server-message', `En attente du tour de ${socket.username}...`);
        }
    });

    socket.on("game-play", (node) => {
        if (!socket.room)   return;

        if (socket.room.game.play(socket.room.pid, node.x, node.y)) {
            socket.room.opponent.emit('server-message', 'A vous de jouer !');
            socket.emit('server-message', `En attente du tour de ${socket.room.opponent.username}...`);

            node.symbol = (socket.room.game.current) ? 'cross' : 'circle';
            socket.emit("game-play-confirm", node);
            socket.room.opponent.emit("game-play-confirm", node);
        }

        let p = socket.room.game.isFinished();
        if (p >= 0) {
            if (p == 2) {
                socket.emit('game-finish', 2);
                socket.room.opponent.emit('game-finish', 2);
                socket.emit('server-message', 'Match nul !');
                socket.room.opponent.emit('server-message', 'Match nul !');
            } else {
                socket.emit('game-finish', p == socket.room.pid);
                socket.room.opponent.emit('game-finish', p == socket.room.opponent.pid);

                let winnerName = (socket.room.pid == p) ? socket.username : socket.room.opponent.username;
                socket.emit('server-message', `${winnerName} a gagné la partie !`);
                socket.room.opponent.emit('server-message', `${winnerName} a gagné la partie !`);
            }

            socket.room.opponent.room = undefined;
            socket.room = undefined;
        }
    })
});